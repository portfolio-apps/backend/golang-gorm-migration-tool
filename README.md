# Golang Gorm Migration Tool (WIP)

### Build Infrastructure
```bash
docker-compose up --build
```

### Run migrations
```bash
go run main.go migrate
```

### Create Migration
```bash
go run main.go create migration <things> <create_things_tables>
```

### Run Seeds
```bash
go run main.go migrate seed
```
### Rollback last migration
```bash
go run main.go migrate rollback
```

### Fresh database
```bash
go run main.go migrate fresh
```
