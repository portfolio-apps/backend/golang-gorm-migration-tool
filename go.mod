module project

go 1.16

require (
	gorm.io/driver/mysql v1.3.4
	gorm.io/gorm v1.23.8
)
