package db

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Connect() (*gorm.DB) {
	dsn := "gomigrate:test1234@tcp(127.0.0.1:3306)/gomigrate?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	return db
}

func Migrate(active bool) {
	if active == true {
		AddMigrationRecords()
	}
}

func Seed(active bool) {
	if active == true {
		FetchSeeders()
	}
}

func Fresh(active bool) {
	if active == true {
		FreshDatabase()
	}
}