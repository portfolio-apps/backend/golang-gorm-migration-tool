package models

type User struct {
	ID             uint         `json:"id" gorm:"primary_key"`
	Name           string       `json:"name" binding:"required"`
	Username       string       `json:"username" binding:"required" gorm:"unique"`
	Email          string       `json:"email" binding:"required" gorm:"unique"`
	Password       string       `json:"password" binding:"required,min=8"`
}