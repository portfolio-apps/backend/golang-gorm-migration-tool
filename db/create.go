package db

import (
	"fmt"
	"log"
	"os"
	"time"
)

func CreateMigration() {
	tNow := time.Now()
	tUnix := tNow.Unix()
	tUnixString := fmt.Sprint(tUnix)

	var url string = "db/migrations/sql/" + tUnixString + "_" + os.Args[4] + ".up.sql"
	f, err := os.Create(url)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	migrationTxt := fmt.Sprintf(`CREATE TABLE %[1]v (
	id BIGINT primary key,
	name TEXT not null,
	created_at TIMESTAMP default now()
);`, os.Args[3])

	_, err2 := f.WriteString(migrationTxt)
	if err2 != nil {
		panic(err2)
	}

	url = "db/migrations/sql/" + tUnixString + "_" + os.Args[4] + ".down.sql"
	f, err = os.Create(url)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	migrationTxt = fmt.Sprintf(`DROP TABLE %[1]v;`, os.Args[3])

	_, err2 = f.WriteString(migrationTxt)
	if err2 != nil {
		panic(err2)
	}
}