CREATE TABLE migrations (
	id BIGINT primary key AUTO_INCREMENT,
	migration TEXT not null,
    batch INT not null default 1,
	created_at TIMESTAMP default now()
);