CREATE TABLE organizations (
	id BIGINT primary key,
	name TEXT not null,
	created_at TIMESTAMP default now()
);