CREATE TABLE users (
	id BIGINT primary key AUTO_INCREMENT,
	name TEXT not null,
	email TEXT not null,
	password TEXT not null,
	username TEXT not null,
	created_at TIMESTAMP default now()
);