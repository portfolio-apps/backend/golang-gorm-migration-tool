package db

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	models "project/db/models"
	"strings"
)

func AddMigrationRecords() {
	db := Connect()

	files, err := filepath.Glob("db/migrations/sql/*.up.sql")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		filename := strings.Split(file, "/")
		migration := strings.ReplaceAll(filename[3], ".up.sql", "")
		mig := models.Migration{Migration: migration, Batch: 1}
		db.Create(&mig)
	}
}

func FreshDatabase() {
	db := Connect()
	db.Exec("DROP DATABASE IF EXISTS gomigrate")
	db.Exec("CREATE DATABASE gomigrate")
}

func UpSql() {
	db := Connect()
	files, err := filepath.Glob("db/migrations/sql/*.up.sql")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {

		fmt.Println(">> Migrating: " + file)

		file_data, err := ioutil.ReadFile(file)
		if err != nil {
			fmt.Println(err)
		}
		db.Exec(string(file_data))
	}
}

func reverse(s []string) ([]string) {
    for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
        s[i], s[j] = s[j], s[i]
    }
	return s
}

func DownSql() {
	db := Connect()
	files, err := filepath.Glob("db/migrations/sql/*.down.sql")
	if err != nil {
		log.Fatal(err)
	}

	files = reverse(files)

	for _, file := range files {

		fmt.Println(">> Undo: " + file)

		file_data, err := ioutil.ReadFile(file)
		if err != nil {
			fmt.Println(err)
		}
		db.Exec(string(file_data))
	}
}

func Rollback() {
	db := Connect()
	files, err := filepath.Glob("db/migrations/sql/*.down.sql")
	if err != nil {
		log.Fatal(err)
	}

	files = reverse(files)
	lastFile := files[0]

	fmt.Println(">> Rollback Last: " + lastFile)
	file_data, err := ioutil.ReadFile(lastFile)
	if err != nil {
		fmt.Println(err)
	}
	db.Exec(string(file_data))
}
