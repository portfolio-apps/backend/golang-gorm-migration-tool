package seeders

import (
	"gorm.io/gorm"
	models "project/db/models"
)

func SeedUsers(db *gorm.DB) {
	user := models.User{Name: "Jinzhu", Username: "test.test", Email: "hello@test.com", Password: "test1234"}
	user2 := models.User{Name: "Ryan", Username: "john.test", Email: "john@test.com", Password: "test1234"}

	db.Create(&user)
	db.Create(&user2)
}