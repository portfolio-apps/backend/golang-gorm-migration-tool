package db

import (
	seeders "project/db/seeders"
)

func FetchSeeders() {
	db := Connect()

	seeders.SeedUsers(db)
}