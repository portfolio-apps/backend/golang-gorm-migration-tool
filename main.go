package main

import (
	"fmt"
	"os"

	db "project/db"
)

func main() {

	argsWithoutProg := os.Args[1:]
	fmt.Println(argsWithoutProg)
	// fmt.Println(len(os.Args))

	if os.Args[1] == "migrate" && len(os.Args) == 2 {
		db.UpSql()
		db.Migrate(true)
	}

	if os.Args[1] == "migrate" && len(os.Args) > 2 && os.Args[2] == "up" {
		db.UpSql()
	}

	if os.Args[1] == "migrate" && len(os.Args) > 2 && os.Args[2] == "down" {
		db.DownSql()
	}

	if os.Args[1] == "migrate" && len(os.Args) > 2 && os.Args[2] == "rollback" {
		db.Rollback()
	}
	
	if os.Args[1] == "migrate" && len(os.Args) > 2 && os.Args[2] == "seed" {
		db.Seed(true)
	} 

	if os.Args[1] == "migrate" && len(os.Args) > 2 && os.Args[2] == "fresh" {
		db.Fresh(true)
	} 

	if os.Args[1] == "create" {
		if os.Args[2] == "migration" {
			db.CreateMigration()
		}
	}

	fmt.Println("Migrations finished...")
}