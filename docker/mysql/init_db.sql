-- Create Databases
CREATE DATABASE IF NOT EXISTS `gomigrate`;
CREATE DATABASE IF NOT EXISTS `gomigrate_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'gomigrate'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;
